structure Hello =
struct
  val name = "bob"

  fun sayHello () =
    let val s = "Hello " ^ name ^ "!\n"
    in print s
    end
end
