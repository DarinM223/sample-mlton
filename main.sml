structure Temp =
struct
  val counter = ref ~1

  fun fresh () =
    (counter := !counter + 1; !counter)
end

val _ =
  let
    val _ = Hello.sayHello ()
    val seed = (521, 432)
    val r = Random.rand seed
    val i = Random.randInt r
    val a = DynamicArray.array (1000, 0)
    val j = DynamicArray.sub (a, 1001)
    val _ = print "Hello world\n"
    val _ = Foo.puts "hello"
    val arr = Array.array (10, 0)
    open JSONParser
    val tree = parse (openString "{\"a\": [1, 2, 3]}")
    val _ = JSONPrinter.print (TextIO.stdOut, tree)
  in
    print ("i: " ^ Int.toString i ^ " j: " ^ Int.toString j ^ "\n");
    print ((Int.toString (Array.sub (arr, 5))) ^ "\n")
  end
    (* structure Foo =
            struct
              fun run () =
                let
                  open CML
                  val ch1 = channel ()
                  val ch2 = channel ()
                  val () = send (ch1, 1)
                  val () = send (ch2, "hi")
                in
                  select
                    [ wrap (recvEvt ch1, fn a =>
                        print ("Received " ^ Int.toString a ^ "\n"))
                    , wrap (recvEvt ch2, fn a => print ("Received " ^ a ^ "\n"))
                    ]
                end

              val _ = let in RunCML.doit (run, NONE) end
            end
        *)
